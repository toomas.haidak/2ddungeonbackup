using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
// using UnityEditor.Connect;
using UnityEngine;
using UnityEngine.UI;


public class Unit : MonoBehaviour {
    [SerializeField] public LayerMask tileLayer;

    public bool selected;
    public int tileSpeed;
    public bool hasMoved;

    public float moveSpeed;

    public int playerNumber;

    public Button kingAttackButton;

    public int attackRange;
    private List<Unit> enemyInRange = new List<Unit>();
    public bool hasAttacked;

    public GameObject weaponIcon;
    public GameObject attackIconN;
    public GameObject attackIconS;
    public GameObject attackIconW;
    public GameObject attackIconE;
    public int health;
    public int attackDamage;
    public int defenseDamage;
    public int armor;

    public DamageIcon damageIcon;

    public int kingHealth;
    public bool isKing;

    GameMaster gm;
    Movement mv;

    void Start() {
        Debug.Log("Starting Unit Start function");
        gm = FindObjectOfType<GameMaster>();
        // mv = GameObject.FindObjectOfType<Unit>().GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update() {
    }

    private void OnMouseDown() {
        Debug.Log("Clicked on unit. Transfrom position is " + transform.position);
        ResetWeaponIcon();
        if (selected == true) {
            selected = false;
            gm.selectedUnit = null;
            gm.resetTiles();
        }
        else {
            if (playerNumber == gm.playerTurn && !hasMoved) {
                if (gm.selectedUnit != null) {
                    gm.selectedUnit.selected = false;
                }

                gm.selectedUnit = this;
                gm.ToggleMainHeroStatsPanel(this);
                // Debug.Log("Before reset tiles. Transfrom position is " + transform.position);
                gm.resetTiles();
                // Debug.Log("After reset tiles. Transfrom position is " + transform.position);
                mv = this.GetComponent<Movement>();
                // Debug.Log("After getComponent transform position is" + transform.position);
                mv.FindSelectableTiles(this);
                if (!hasAttacked) {
                    GetEnemies();
                }
                // GetWalkableTiles();
            }
            else {
                if (playerNumber == gm.playerTurn) {
                    gm.ToggleMainHeroStatsPanel(this);
                }
                else {
                    gm.ToggleMainNPCStatsPanel(this);
                }
            }
        }

        if (gm.selectedUnit != null) {
            Collider2D col = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.15f);
            Unit secondClickunit = col.GetComponent<Unit>();
            if (gm.selectedUnit != secondClickunit) {
                if (gm.selectedUnit.enemyInRange.Contains(secondClickunit) && gm.selectedUnit.hasAttacked == false) {
                    gm.selectedUnit.Attack(secondClickunit);
                    gm.resetTiles();
                    gm.selectedUnit.hasMoved = true;
                }
            }
        }
        else {
            gm.ToggleMainNPCStatsPanel(this);
        }
    }

    private void OnMouseOver() {
        if (Input.GetMouseButtonDown(1)) {
            gm.toggleStatsPanel(this);
        }
    }

    public void Attack(Unit enemy) {
        hasAttacked = true;
        int enemyDamage = attackDamage - enemy.armor;
        if (enemyDamage >= 1) {
            DamageIcon instance = Instantiate(damageIcon, enemy.transform.position, Quaternion.identity);
            instance.Setup(enemyDamage);
            enemy.health -= enemyDamage;
            gm.UpdateHeroStatsPanel();
            gm.UpdateNPCStatsPanel();
        }

        if (enemy.health <= 0) {
            Destroy(enemy.gameObject);
            gm.RemoveStatsPanel(enemy);
            gm.CloseMainStatsPanels();
        }

        PushEnemy(enemy);
    }

    public void CalculateKingAttackTiles() {
        gm.resetTiles();
        List<RaycastHit2D> hitlist = new List<RaycastHit2D>();
        Physics2D.Raycast(transform.position + Vector3.up * 2, Vector2.up, new ContactFilter2D(), hitlist, 4f);
        if (hitlist.Count > 0) {
        }
        foreach (RaycastHit2D hit in hitlist) {
            if (hit.collider.GetComponent<Tile>()) {
                Tile tile = hit.collider.GetComponent<Tile>();
                tile.kingAttackable = true;
            }
        }
        
       hitlist = new List<RaycastHit2D>();
        Physics2D.Raycast(transform.position - Vector3.up * 2, Vector2.down, new ContactFilter2D(), hitlist, 4f);
        if (hitlist.Count > 0) {
        }
        foreach (RaycastHit2D hit in hitlist) {
            if (hit.collider.GetComponent<Tile>()) {
                Tile tile = hit.collider.GetComponent<Tile>();
                tile.kingAttackable = true;
            }
        }
        
        hitlist = new List<RaycastHit2D>();
        Physics2D.Raycast(transform.position + Vector3.right * 2, Vector2.right, new ContactFilter2D(), hitlist, 4f);
        if (hitlist.Count > 0) {
        }
        foreach (RaycastHit2D hit in hitlist) {
            if (hit.collider.GetComponent<Tile>()) {
                Tile tile = hit.collider.GetComponent<Tile>();
                tile.kingAttackable = true;
            }
        }
        
        hitlist = new List<RaycastHit2D>();
        Physics2D.Raycast(transform.position - Vector3.right * 2, Vector2.left, new ContactFilter2D(), hitlist, 4f);
        if (hitlist.Count > 0) {
        }
        foreach (RaycastHit2D hit in hitlist) {
            if (hit.collider.GetComponent<Tile>()) {
                Tile tile = hit.collider.GetComponent<Tile>();
                tile.kingAttackable = true;
            }
        }
    }

    public void PushEnemy(Unit enemy) {
        float xvalue = transform.position.x - enemy.transform.position.x;
        float yvalue = transform.position.y - enemy.transform.position.y;

        if (Math.Abs(xvalue) < 0.1 && yvalue > 0.8) {
            Debug.Log(" X valu is " + xvalue);
            Debug.Log(" Y valu is " + yvalue);
            PushEnemyDown(enemy);
        }

        if (Math.Abs(xvalue) < 0.1 && yvalue < -0.8) {
            Debug.Log(" X valu is " + xvalue);
            Debug.Log(" Y valu is " + yvalue);
            PushEnemyUp(enemy);
            
        }

        if (xvalue > 0.8 && Math.Abs(yvalue) < 0.1) {
            Debug.Log(" X valu is " + xvalue);
            Debug.Log(" Y valu is " + yvalue);
            PushEnemyLeft(enemy);
            
        }

        if (xvalue < -0.8 && Math.Abs(yvalue) < 0.1) {
            Debug.Log(" X valu is " + xvalue);
            Debug.Log(" Y valu is " + yvalue);
            Debug.Log("This is what happens");
            PushEnemyRight(enemy);
            
        }
    }

    public void PushEnemyDown(Unit enemy) {
        Vector2 pushTarget = enemy.transform.position - Vector3.up;
        RaycastHit2D hit = Physics2D.Raycast(pushTarget, Vector2.up, 0.6f, 1 << 9);
        if (!hit.collider) {
            enemy.transform.position -= Vector3.up;
        }
        else {
            DamageIcon instance = Instantiate(damageIcon, enemy.transform.position, Quaternion.identity);
            instance.Setup(1);
            enemy.health--;
            gm.UpdateHeroStatsPanel();
            gm.UpdateNPCStatsPanel();

            if (enemy.health <= 0) {
                Destroy(enemy.gameObject);
                gm.RemoveStatsPanel(enemy);
            }

            Unit unit = hit.collider.GetComponent<Unit>();
            if (unit) {
                DamageIcon instance2 = Instantiate(damageIcon, unit.transform.position, Quaternion.identity);
                instance2.Setup(1);
                unit.health--;
                gm.UpdateHeroStatsPanel();
                gm.UpdateNPCStatsPanel();
                if (unit.health <= 0) {
                    Destroy(unit.gameObject);
                    gm.RemoveStatsPanel(unit);
                }
            }
        }
    }

    public void PushEnemyUp(Unit enemy) {
        Vector2 pushTarget = enemy.transform.position + Vector3.up;
        RaycastHit2D hit = Physics2D.Raycast(pushTarget, Vector2.up, 0.6f, 1 << 9);
        if (!hit.collider) {
            enemy.transform.position += Vector3.up;
        }
        else {
            DamageIcon instance = Instantiate(damageIcon, enemy.transform.position, Quaternion.identity);
            instance.Setup(1);
            enemy.health--;
            gm.UpdateHeroStatsPanel();
            gm.UpdateNPCStatsPanel();
            if (enemy.health <= 0) {
                Destroy(enemy.gameObject);
                gm.RemoveStatsPanel(enemy);
            }

            Unit unit = hit.collider.GetComponent<Unit>();
            if (unit) {
                DamageIcon instance2 = Instantiate(damageIcon, unit.transform.position, Quaternion.identity);
                instance2.Setup(1);
                unit.health--;
                gm.UpdateHeroStatsPanel();
                gm.UpdateNPCStatsPanel();
                if (unit.health <= 0) {
                    Destroy(unit.gameObject);
                    gm.RemoveStatsPanel(unit);
                }
            }
        }
    }

    public void PushEnemyRight(Unit enemy) {
        Vector2 pushTarget = enemy.transform.position + Vector3.right;
        RaycastHit2D hit = Physics2D.Raycast(pushTarget, Vector2.up, 0.6f, 1 << 9);
        if (!hit.collider) {
            Debug.Log("No hit to the right");
            enemy.transform.position += Vector3.right;
        }
        else {
            DamageIcon instance = Instantiate(damageIcon, enemy.transform.position, Quaternion.identity);
            instance.Setup(1);
            enemy.health--;
            if (enemy.health <= 0) {
                Destroy(enemy.gameObject);
                gm.RemoveStatsPanel(enemy);
            }

            Unit unit = hit.collider.GetComponent<Unit>();
            if (unit) {
                DamageIcon instance2 = Instantiate(damageIcon, unit.transform.position, Quaternion.identity);
                instance2.Setup(1);
                unit.health--;
                if (unit.health <= 0) {
                    Destroy(unit.gameObject);
                    gm.RemoveStatsPanel(unit);
                }
            }
        }
    }

    public void PushEnemyLeft(Unit enemy) {
        Vector2 pushTarget = enemy.transform.position - Vector3.right;
        RaycastHit2D hit = Physics2D.Raycast(pushTarget, Vector2.up, 0.6f, 1 << 9);
        if (!hit.collider) {
            enemy.transform.position -= Vector3.right;
        }
        else {
            DamageIcon instance = Instantiate(damageIcon, enemy.transform.position, Quaternion.identity);
            instance.Setup(1);
            enemy.health--;
            if (enemy.health <= 0) {
                Destroy(enemy.gameObject);
                gm.RemoveStatsPanel(enemy);
            }

            Unit unit = hit.collider.GetComponent<Unit>();
            if (unit) {
                DamageIcon instance2 = Instantiate(damageIcon, unit.transform.position, Quaternion.identity);
                instance2.Setup(1);
                unit.health--;
                if (unit.health <= 0) {
                    Destroy(unit.gameObject);
                    gm.RemoveStatsPanel(unit);
                }
            }
        }
    }

    public void GetEnemies() {
        enemyInRange.Clear();
        if (attackRange == 1 && kingHealth == 0) {
            foreach (Unit unit in FindObjectsOfType<Unit>()) {
                if (Mathf.Abs(transform.position.x - unit.transform.position.x) +
                    Mathf.Abs(transform.position.y - unit.transform.position.y) <= attackRange + 0.1) {
                    if (unit.playerNumber != gm.playerTurn && hasAttacked == false) {
                        enemyInRange.Add(unit);
                        unit.weaponIcon.SetActive(true);
                    }
                }
            }
        }

        if (attackRange > 1) {
            RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.up, Vector2.up, 10, 1 << 9);
            if (hit.collider.GetComponent<Unit>() != null) {
                Unit unit = hit.collider.GetComponent<Unit>();
                if (unit.playerNumber != playerNumber) {
                    enemyInRange.Add(unit);
                    unit.weaponIcon.SetActive(true);
                }
            }

            hit = Physics2D.Raycast(transform.position - Vector3.up, Vector2.down, 10, 1 << 9);
            if (hit.collider.GetComponent<Unit>() != null) {
                Unit unit = hit.collider.GetComponent<Unit>();
                if (unit.playerNumber != playerNumber) {
                    enemyInRange.Add(unit);
                    unit.weaponIcon.SetActive(true);
                }
            }

            hit = Physics2D.Raycast(transform.position + Vector3.right, Vector2.right, 10, 1 << 9);
            if (hit.collider.GetComponent<Unit>() != null) {
                Unit unit = hit.collider.GetComponent<Unit>();
                if (unit.playerNumber != playerNumber) {
                    enemyInRange.Add(unit);
                    unit.weaponIcon.SetActive(true);
                }
            }

            hit = Physics2D.Raycast(transform.position - Vector3.right, Vector2.left, 10, 1 << 9);
            if (hit.collider.GetComponent<Unit>() != null) {
                Unit unit = hit.collider.GetComponent<Unit>();
                if (unit.playerNumber != playerNumber) {
                    enemyInRange.Add(unit);
                    unit.weaponIcon.SetActive(true);
                }
            }
        }
    }

    public void ResetWeaponIcon() {
        foreach (Unit unit in FindObjectsOfType<Unit>()) {
            unit.weaponIcon.SetActive(false);
        }
    }

    public void ShowAttack(Unit nearestTarget, float range) {
        Debug.Log("Range is " + range);
        RaycastHit2D hitN = Physics2D.Raycast(transform.position + Vector3.up, Vector2.up, range, 1 << 9);
        if (hitN.collider) {
            if (hitN.collider.GetComponent<Unit>() != null) {
                Unit targetUnit = hitN.collider.GetComponent<Unit>();
                if (targetUnit == nearestTarget) {
                    attackIconN.SetActive(true);
                }
            }
        }

        RaycastHit2D hitS = Physics2D.Raycast(transform.position - Vector3.up, Vector2.down, distance: range, 1 << 9);
        if (hitS.collider) {
            if (hitS.collider.GetComponent<Unit>() != null) {
                Unit targetUnit = hitS.collider.GetComponent<Unit>();
                if (targetUnit == nearestTarget) {
                    attackIconS.SetActive(true);
                }
            }
        }

        RaycastHit2D hitE =
            Physics2D.Raycast(transform.position + Vector3.right, Vector2.right, distance: range, 1 << 9);
        if (hitE.collider) {
            if (hitE.collider.GetComponent<Unit>() != null) {
                Unit targetUnit = hitE.collider.GetComponent<Unit>();
                if (targetUnit == nearestTarget) {
                    attackIconE.SetActive(true);
                }
            }
        }

        RaycastHit2D hitW =
            Physics2D.Raycast(transform.position - Vector3.right, Vector2.left, distance: range, 1 << 9);
        if (hitW.collider) {
            if (hitW.collider.GetComponent<Unit>() != null) {
                Unit targetUnit = hitW.collider.GetComponent<Unit>();
                if (targetUnit == nearestTarget) {
                    attackIconW.SetActive(true);
                }
            }
        }
    }

    public void NPCAttackN() {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.up, Vector2.up, attackRange, 1 << 9);
        if (hit.collider) {
            if (hit.collider.GetComponent<Unit>() != null) {
                Unit targetUnit = hit.collider.GetComponent<Unit>();
                attackIconN.transform.position += Vector3.up;
                attackIconN.transform.position -= Vector3.up;
                Attack(targetUnit);
            }
        }
    }

    public void NPCAttackS() {
        RaycastHit2D hit = Physics2D.Raycast(transform.position - Vector3.up, Vector2.down, attackRange, 1 << 9);
        if (hit.collider) {
            if (hit.collider.GetComponent<Unit>() != null) {
                Unit targetUnit = hit.collider.GetComponent<Unit>();
                attackIconN.transform.position -= Vector3.up;
                attackIconN.transform.position += Vector3.up;
                Attack(targetUnit);
            }
        }
    }

    public void NPCAttackE() {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.right, Vector2.right, attackRange, 1 << 9);
        if (hit.collider) {
            if (hit.collider.GetComponent<Unit>() != null) {
                Unit targetUnit = hit.collider.GetComponent<Unit>();
                attackIconN.transform.position += Vector3.right;
                attackIconN.transform.position -= Vector3.right;
                Attack(targetUnit);
            }
        }
    }

    public void NPCAttackW() {
        RaycastHit2D hit = Physics2D.Raycast(transform.position - Vector3.right, Vector2.left, attackRange, 1 << 9);
        if (hit.collider) {
            if (hit.collider.GetComponent<Unit>() != null) {
                Unit targetUnit = hit.collider.GetComponent<Unit>();
                attackIconN.transform.position -= Vector3.right;
                attackIconN.transform.position += Vector3.right;
                Attack(targetUnit);
            }
        }
    }
}