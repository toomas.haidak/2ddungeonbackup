using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NPCMovement : Movement {
    public Unit nearestTarget;

    private Tile actualTargetTile;

    public GameMaster gm;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void FindNearestTarget(Unit unit, Action onComplete) {
        GameObject[] targets = GameObject.FindGameObjectsWithTag("Player");

        GameObject nearest = null;
        float distance = Mathf.Infinity;

        foreach (GameObject obj in targets) {
            float d = Vector3.Distance(transform.position, obj.transform.position);


            if (d < distance) {
                distance = d;
                nearest = obj;
            }
        }

        if (targets.Length > 0) {
            nearestTarget = nearest.GetComponent<Unit>();
            Debug.Log("Lähim Player unit on " + nearestTarget.name);

            CalculatePath(unit, onComplete);
        }
        else {
            gm = FindObjectOfType<GameMaster>();
            gm.EndScreenPanel.SetActive(true);
        }
    }

    void CalculatePath(Unit unit, Action onComplete) {
        Tile targetTile = GetTargetTile(nearestTarget);
        FindPath(targetTile, unit);
        
        StartMove(onComplete);
    }

    protected Tile FindLowestF(List<Tile> list) {
        Tile lowest = list[0];

        foreach (Tile t in list) {
            if (t.f < lowest.f) {
                lowest = t;
            }
        }

        list.Remove(lowest);

        return lowest;
    }

    protected Tile FindEndTile(Tile t, Unit unit) {
        Debug.Log("Started calculating end tile");
        Stack<Tile> tempPath = new Stack<Tile>();

        Tile next = t.parent;
        while (next != null) {
            tempPath.Push(next);
            next = next.parent;
            Debug.Log("Next one is " + next);
        }

        if (tempPath.Count <= unit.tileSpeed) {
            return t.parent;
        }

        Tile endTile = null;
        for(int i = 0; i <= unit.tileSpeed; i++) {
            endTile = tempPath.Pop();
        }
        Debug.Log("End tile on " + endTile.name);
        return endTile;
    }

   protected void FindPath(Tile target, Unit unit) {
        ComputeAdjacencyList(target);
        GetCurrentTile(unit);
    
        List<Tile> openList = new List<Tile>();
        List<Tile> closedList = new List<Tile>();
   
        openList.Add(currentTile);
        Debug.Log("Open and closed list initiated for unit " + name);
        // currentTile = ??
        currentTile.h = Vector3.Distance(currentTile.transform.position, target.transform.position);
        Debug.Log("current.Tile.h = " + currentTile.h);
        currentTile.f = currentTile.h;
    
        while (openList.Count > 0) {
            Tile t = FindLowestF(openList);
            closedList.Add(t);
            // Debug.Log("Target tile on " + target.name + target.transform.position);
            // Debug.Log("Find lowestF tile t on " + t.name + t.transform.position);
    
            if (t == target) {
                Debug.Log("FindLowestF found that  t="+ t);
                Debug.Log("Closed list has " + closedList.Count + " elements");
                actualTargetTile = FindEndTile(t, unit);
                CreatePathToMove(actualTargetTile);
                return;
            }
            // Debug.Log("t ei võrdunud Targetiga");
            
            foreach (Tile tile in t.adjacencyList) {
                if (closedList.Contains(tile)) {
                    //Do nothing, tile is already processed
                } else if (openList.Contains(tile)) {
                    float tempG = t.g + Vector3.Distance(tile.transform.position, t.transform.position);
    
                    if (tempG < tile.g) {
                        tile.parent = t;
    
                        tile.g = tempG;
                        tile.f = tile.g + tile.h;
                    }
                }
                else {
                    tile.parent = t;
                    tile.g = t.g + Vector3.Distance(tile.transform.position, t.transform.position);
                    tile.h = Vector3.Distance(tile.transform.position, target.transform.position);
                    
                    openList.Add(tile);
                }
                
            }
        }

   }
}
