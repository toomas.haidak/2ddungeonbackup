using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class Tile : MonoBehaviour {
    public bool current = false;
    public bool target = false;
    public bool selectable = false;
    public bool kingAttackable = false;

    public List<Tile> adjacencyList = new List<Tile>();

    //Need for BFS (breadth first search)
    private List<Collider2D> collisions;
    public bool visited = false;
    public Tile parent = null;
    public int distance = 0;

    private SpriteRenderer rend;
    public Sprite[] tileGraphics;
    public float hoverAmount;
    public LayerMask obstacleLayer;
    public Color highlightedColor;
    public bool isWalkable;

    // Needed for A*
    public float f = 0;
    public float g = 0;
    public float h = 0;

    private GameMaster gm;
    Movement mv;

    public Color creatableColor;
    public bool isCreatable;

    private void Start() {
        Debug.Log("Starting Tile Start function");
        rend = GetComponent<SpriteRenderer>();
        int randTile = Random.Range(0, tileGraphics.Length);
        rend.sprite = tileGraphics[randTile];

        gm = FindObjectOfType<GameMaster>();
        // mv = GameObject.FindObjectOfType<Unit>().GetComponent<Movement>();
    }

    void Update() {
        if (current) {
            rend.color = Color.grey;
        }
        else if (target) {
            rend.color = Color.magenta;
        }
        else if (selectable) {
            rend.color = Color.red;
        }
        else if (kingAttackable) {
            rend.color = Color.red;
        }
    }

    private void OnMouseEnter() {
        transform.localScale += Vector3.one * hoverAmount;
    }

    private void OnMouseExit() {
        transform.localScale -= Vector3.one * hoverAmount;
    }

    public bool IsClear() {
        Collider2D obstacle = Physics2D.OverlapCircle(transform.position, 0.2f, obstacleLayer);
        if (obstacle != null) {
            return false;
        }
        else {
            return true;
        }
    }

    public void Highlight() {
        rend.color = highlightedColor;
        isWalkable = true;
    }

    public void Reset() {
        rend.color = Color.white;
        isWalkable = false;
        isCreatable = false;

        adjacencyList.Clear();
        current = false;
        target = false;
        selectable = false;
        kingAttackable = false;
        visited = false;
        parent = null;
        distance = 0;
    }

    public void FindNeighbours(Tile target) {
        // Reset();
        // Debug.Log("Casting ray from position " + (transform.position+Vector3.up));
        CheckTile(Vector3.up, target);
        // Debug.Log("Casting ray from position " + (transform.position-Vector3.up));
        CheckTile(-Vector3.up, target);
        // Debug.Log("Casting ray from position " + (transform.position+Vector3.right));
        CheckTile(Vector3.right, target);
        // Debug.Log("Casting ray from position " + (transform.position-Vector3.right));
        CheckTile(-Vector3.right, target);
    }

    public void CheckTile(Vector3 direction, Tile target) {
        Tile tile = null;
        List<RaycastHit2D> hitlist = new List<RaycastHit2D>();
        Physics2D.Raycast(transform.position + direction, Vector2.up, new ContactFilter2D(), hitlist, 0.55f);

        if ((hitlist.Exists(hit2D => hit2D.collider.GetComponent<Tile>() != null) &&
             hitlist.TrueForAll(hit2D => hit2D.collider.GetComponent<Unit>() == null)) ||
            hitlist.Exists(hit2D => hit2D.collider.GetComponent<Tile>() == target) && target != null) {
            tile = hitlist.Find(hit2D => hit2D.collider.GetComponent<Tile>() != null).collider.GetComponent<Tile>();
            adjacencyList.Add(tile);
        }
        // RaycastHit2D hit = Physics2D.Raycast(transform.position + direction, (Vector2.up), 1f);
        // if (hit) {
        //     tile = hit.collider.GetComponent<Tile>();
        //     if (tile != null) {
        //         Debug.Log("Adding tile " + tile.name + " " + tile.transform.position + " to adjacency list");
        //         adjacencyList.Add(tile);
        //     }
        //     else {
        //         Debug.Log("Did not have adjacent tile");
        //     }
        // }
    }

    public void SetCreatable() {
        rend.color = creatableColor;
        isCreatable = true;
    }

    private void OnMouseDown() {
        if (selectable && gm.selectedUnit != null) {
            mv = gm.selectedUnit.GetComponent<Movement>();
            mv.CreatePathToMove(this);
        }

        if (kingAttackable) {
            RaycastHit2D hit = new RaycastHit2D();
            hit = Physics2D.Raycast(transform.position + Vector3.up, Vector2.up, 0.6f, 1 << 9);
            if (hit) {
                if (hit.collider.GetComponent<Unit>() != null) {
                    Unit pushedUnit = hit.collider.GetComponent<Unit>();
                    pushedUnit.PushEnemyUp(pushedUnit);
                }
            }

            hit = Physics2D.Raycast(transform.position - Vector3.up, Vector2.down, 0.6f, 1<<9);
            if (hit) {
                if (hit.collider.GetComponent<Unit>() != null) {
                    Unit pushedUnit = hit.collider.GetComponent<Unit>();
                    pushedUnit.PushEnemyDown(pushedUnit);
                }
            }


            hit = Physics2D.Raycast(transform.position + Vector3.right, Vector2.right, 0.6f, 1 << 9);
            if (hit) {
                if (hit.collider.GetComponent<Unit>() != null) {
                    Unit pushedUnit = hit.collider.GetComponent<Unit>();
                    pushedUnit.PushEnemyRight(pushedUnit);
                }
            }

            hit = Physics2D.Raycast(transform.position - Vector3.right, Vector2.left, 0.6f, 1 << 9);
            if (hit) {
                if (hit.collider.GetComponent<Unit>() != null) {
                    Unit pushedUnit = hit.collider.GetComponent<Unit>();
                    pushedUnit.PushEnemyLeft(pushedUnit);
                }
            }

            gm.selectedUnit.hasAttacked = true;
            gm.resetTiles();
        }
    }
}