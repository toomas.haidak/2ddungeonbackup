﻿using System;
using UnityEngine;

namespace DefaultNamespace {
    public class TestCallback : MonoBehaviour {
        private void Start() {
            Debug.Log("1");
            TestMethod1(CallbackMethod);
        }

        private void CallbackMethod() {
            Debug.Log("3");
        }

        private void TestMethod1(Action callback) {
            Debug.Log("2");
            callback.Invoke();
        }
    }
}