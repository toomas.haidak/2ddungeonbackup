using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameMaster : MonoBehaviour {

    public GameObject mainHeroStatsPanel;
    public GameObject mainNPCStatsPanel;
    public GameObject EndScreenPanel;
    
    public Unit selectedUnit;
    public int playerTurn = 1;

    public GameObject selectedUnitSquare;

    public Image playerIndicator;
    public Sprite player1Indicator;
    public Sprite player2Indicator;

    public int player1Gold = 100;
    public int player2Gold = 100;

    public Text player1GoldText;
    public Text player2GoldText;

    public BarrackItem purchasedItem;

    public GameObject statsPanel;
    public GameObject NPCStatsPanel;
    public Vector2 statsPanelShift;
    public Unit viewedUnit;
    public Unit viewedNPCUnit;

    public Text healthText;
    public Text armorText;
    public Text attackDamageText;
    
    public Text NPCHealthText;
    public Text NPCArmorText;
    public Text NPCAttackDamageText;

    public Unit[] spawnableUnits;
    
    public Text defenseDamageText;

    // public void UpdateGoldText() {
    //     player1GoldText.text = player1Gold.ToString();
    //     player2GoldText.text = player2Gold.ToString();
    // }

    // void GetGoldIncome(int playerTurn) {
    //     foreach (Village village in FindObjectsOfType<Village>()) {
    //         if (village.playerNumber == playerTurn) {
    //             if (playerTurn == 1) {
    //                 player1Gold += village.goldPerTurn;
    //             }
    //             else {
    //                 player2Gold += village.goldPerTurn;
    //             }
    //         }
    //     }
    //
    //     UpdateGoldText();
    // }

    public void resetTiles() {
        Tile[] selectedTile = FindObjectsOfType<Tile>();
        var enumerable = selectedTile.Where(tile => tile.enabled);

        foreach (Tile tile in enumerable) {
            tile.Reset();
        }
    }

    public void toggleStatsPanel(Unit unit) {
        if (unit.Equals(viewedUnit) == false) {
            statsPanel.SetActive(true);
            statsPanel.transform.position = (Vector2) unit.transform.position + statsPanelShift;
            viewedUnit = unit;
            UpdateHeroStatsPanel();
        }
        else {
            statsPanel.SetActive(false);
            viewedUnit = null;
        }
    }

    public void UpdateHeroStatsPanel() {
        if (viewedUnit != null) {
            Debug.Log("Viewd unit on " + viewedUnit.name);
            healthText.text = viewedUnit.health.ToString();
            armorText.text = viewedUnit.armor.ToString();
            attackDamageText.text = viewedUnit.attackDamage.ToString();
        }
    }
    
    public void UpdateNPCStatsPanel() {
        if (viewedNPCUnit != null) {
            Debug.Log("Viewd unit on " + viewedNPCUnit.name);
            NPCHealthText.text = viewedNPCUnit.health.ToString();
            NPCArmorText.text = viewedNPCUnit.armor.ToString();
            NPCAttackDamageText.text = viewedNPCUnit.attackDamage.ToString();
        }
    }

    public void MoveStatsPanel(Unit unit) {
        if (unit.Equals(viewedUnit)) {
            statsPanel.transform.position = (Vector2) unit.transform.position + statsPanelShift;
        }
    }

    public void RemoveStatsPanel(Unit unit) {
        if (unit.Equals(viewedUnit)) {
            statsPanel.SetActive(false);
            viewedUnit = null;
        }
    }

    // Start is called before the first frame update
    void Start() {
        Debug.Log("Starting GameMaster Start function");
        // GetGoldIncome(1);
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Debug.Log("Pressed space - end turn");
            EndTurn();
        }

        if (selectedUnit != null) {
            selectedUnitSquare.SetActive(true);
            selectedUnitSquare.transform.position = selectedUnit.transform.position;
        }
        else {
            selectedUnitSquare.SetActive(false);
        }
    }

    public void EndTurn() {
        if (playerTurn == 1) {
            playerTurn = 2;
            playerIndicator.sprite = player2Indicator;
        }
        else if (playerTurn == 2) {
            playerTurn = 1;
            playerIndicator.sprite = player1Indicator;
        }

        CloseMainStatsPanels();
        
        // GetGoldIncome(playerTurn);

        if (selectedUnit != null) {
            selectedUnit.selected = false;
            selectedUnit = null;
        }

        resetTiles();

        foreach (Unit unit in FindObjectsOfType<Unit>()) {
            unit.hasMoved = false;
            unit.weaponIcon.SetActive(false);
            unit.hasAttacked = false;
        }

        // GetComponent<Barrack>().closeMenus();

        if (playerTurn == 2) {
            StartNPCTurn();
        }
    }

    void StartNPCTurn() {
        Debug.Log("Starting NPC turn");
        GameObject[] victoryCheck = GameObject.FindGameObjectsWithTag("NPC");
        if (victoryCheck.Length == 0) {
            EndScreenPanel.SetActive(true);
        }
        SpawnEnemy();
        GameObject[] npcList = GameObject.FindGameObjectsWithTag("NPC");
        TryStartNextMovement(npcList, -1);
    }

    void TryStartNextMovement(GameObject[] npcList, int currentNpcIndex) {
        currentNpcIndex++;
        if (currentNpcIndex <= npcList.Length - 1) {
            StartMovement(npcList, currentNpcIndex);
        }
        else {
            GameObject[] victoryCheck = GameObject.FindGameObjectsWithTag("Player");
            if (victoryCheck.Length == 0) {
                EndScreenPanel.SetActive(true);
                Debug.Log("Set endscreenpanel to true");
            }
            EndTurn();
        }
    }

    private void StartMovement(GameObject[] npcList, int npcIndex) {
        GameObject gameObject = npcList[npcIndex];
        Debug.Log("Selected from the array gameobject " + gameObject.name);
        NPCMovement npc = gameObject.GetComponent<NPCMovement>();
        Unit unit = gameObject.GetComponent<Unit>();
        selectedUnit = unit;
        CheckForNPCAttack(unit);
        selectedUnit.attackIconN.SetActive(false);
        selectedUnit.attackIconS.SetActive(false);
        selectedUnit.attackIconE.SetActive(false);
        selectedUnit.attackIconW.SetActive(false);
        Debug.Log("Starting turn of  " + selectedUnit.name);
        npc.FindNearestTarget(unit, () => { TryStartNextMovement(npcList, npcIndex); });
    }

    public void CheckForNPCAttack(Unit unit) {
        if (selectedUnit.attackIconN.activeSelf) {
            unit.NPCAttackN();
        }
        
        if (selectedUnit.attackIconS.activeSelf) {
            unit.NPCAttackS();
        }
        
        if (selectedUnit.attackIconE.activeSelf) {
            unit.NPCAttackE();
        }
        
        if (selectedUnit.attackIconE.activeSelf) {
            unit.NPCAttackW();
        }
    }

    public void ToggleMainHeroStatsPanel(Unit unit) {
        mainHeroStatsPanel.SetActive(true);
        viewedUnit = unit;
        UpdateHeroStatsPanel();
    }
    
    public void ToggleMainNPCStatsPanel(Unit unit) {
        mainNPCStatsPanel.SetActive(true);
        viewedNPCUnit = unit;
        UpdateNPCStatsPanel();
    }

    public void CloseMainStatsPanels() {
        mainHeroStatsPanel.SetActive(false);
        mainNPCStatsPanel.SetActive(false);
    }

    public void SpawnEnemy() {
        int rand = Random.Range(0, spawnableUnits.Length);
        Unit spawnedUnit = spawnableUnits[rand];
        Instantiate(spawnedUnit, new Vector3(-3.5f, 1.5f, 3f), Quaternion.identity);
    }

    public void ClickKingAttack() {
        if (selectedUnit.kingHealth != null) {
            selectedUnit.CalculateKingAttackTiles();
        }
    }
}