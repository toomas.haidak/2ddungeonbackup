using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

public class Movement : MonoBehaviour {
    public bool moving = false;
    public int move = 5;

    // Vector2 velocity = new Vector2();
    // private Vector2 heading = new Vector2();

    List<Tile> selectableTiles = new List<Tile>();
    GameObject[] tiles;

    Stack<Tile> path = new Stack<Tile>();
    public Tile currentTile;

    GameMaster gm;
    Unit unt;


    protected void Init() {
        tiles = GameObject.FindGameObjectsWithTag("Tile");
    
    }


    public void GetCurrentTile(Unit unit) {
        currentTile = GetTargetTile(unit);
        if (currentTile == null) {
            Debug.Log("Made it to here 3");
        }

        currentTile.current = true;
        Debug.Log("CurrentTile on seatud current=true");
    }

    public Tile GetTargetTile(Unit unit) {
        Collider2D collider = Physics2D.OverlapPoint(unit.transform.position, unit.tileLayer);

        // Debug.Log("Aquiered collider of target tile" + collider.name);
        Tile tile = collider.GetComponent<Tile>();
        Debug.Log("Target tile name is " + tile.name);
        return tile;
    }

    public void ComputeAdjacencyList(Tile target) {
        Debug.Log("Computing adjacency list");
        Tile[] selectedTile = FindObjectsOfType<Tile>();
        var enumerable = selectedTile.Where(tile => tile.enabled);
        foreach (Tile t in enumerable) {
            t.FindNeighbours(target);
        }
    }

    public void FindSelectableTiles(Unit unit) {
        Debug.Log("This is the start of FindSelectableTiles method. Transform position is currently " + transform.position);
        ComputeAdjacencyList(null);
        GetCurrentTile(unit);

        Queue<Tile> process = new Queue<Tile>();
        Debug.Log("Adding current tile " + currentTile.name + " with coordinates " + currentTile.transform.position +
                  " to enque. Transform position is currently " + transform.position);
        process.Enqueue(currentTile);
        currentTile.visited = true;

        while (process.Count > 0) {
            Tile t = process.Dequeue();

            selectableTiles.Add(t);
            t.selectable = true;

            if (t.distance < unit.tileSpeed) {
                foreach (Tile tile in t.adjacencyList) {
                    if (!tile.visited) {
                        tile.parent = t;
                        tile.visited = true;
                        tile.distance = 1 + t.distance;
                        process.Enqueue(tile);
                    }
                }
            }
        }
        Debug.Log("Enqueue finished, transform position is " + transform.position);
    }

    public void CreatePathToMove(Tile tile) {
        Debug.Log("CreatePathToMove method started. " + tile.name + "is end of path. Transform position is" + transform.position);
        path.Clear();

        Tile next = tile;
        while (next != null) {
            path.Push(next);
            next = next.parent;
            if (next != null) {
                Debug.Log("Next tile into the path is " + next.name);
            }
        }

        Debug.Log("Path created. Transform position of unit is" + transform.position);
        gm = FindObjectOfType<GameMaster>();
        gm.resetTiles();
        gm.selectedUnit.hasMoved = true;
        if (gm.selectedUnit.playerNumber == 1) {
            StartMove(null);  
        } 
    }

    public void StartMove(Action onComplete) {
        Debug.Log("StartMove method launched. Transform position is " + transform.position + name);
        if (path.Count > 0) {
            Debug.Log("Path has " + path.Count + " elements");
            Tile t = path.Peek();
            Debug.Log("Next tile is " + t.name);
            MoveNext(onComplete);
            // Debug.Log("End of StartMove method, transform position is " + transform.position);
            
        }
        else {
            onComplete.Invoke();
        }
    }

    void MoveNext(Action onComplete) {
        gm = FindObjectOfType<GameMaster>();
        // Debug.Log("MoveNext launches StartMovement method. TRansform position is " + transform.position);
        StartCoroutine(StartMovement(Vector2.zero, onComplete));
    }

    IEnumerator StartMovement(Vector2 tilePos, Action onComplete) {
        int moveSpeed = gm.selectedUnit.tileSpeed;
        tilePos = path.Pop().transform.position;
        // Debug.Log("Unit tilespeed is " + moveSpeed + " position is " + transform.position);
        while (transform.position.x != tilePos.x) {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(tilePos.x, transform.position.y),
                moveSpeed * Time.deltaTime);

            yield return null;
        }

        while (transform.position.y != tilePos.y) {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, tilePos.y),
                moveSpeed * Time.deltaTime);
            yield return null;

        }
        // Debug.Log("Path countis on elemente " + path.Count);
        if (path.Count > 0) {
            Debug.Log("Move to next tile started" + path.Peek().name);
            MoveNext(onComplete);
        }
        else {
            // gm.selectedUnit.hasMoved = true;
            if (gm.selectedUnit.playerNumber == 2) {
                NPCMovement nt = this.GetComponent<NPCMovement>();
                    Unit unt = GetComponent<Unit>();
                    float range = unt.attackRange - 0.5f;
                    unt.ShowAttack(nt.nearestTarget, range);
                    onComplete.Invoke();
            } else {
                unt = gm.selectedUnit.GetComponent<Unit>();
                unt.GetEnemies();
            }
        }
        
        
        // ResetWeaponIcon();
        // GetEnemies();
        // gm.MoveStatsPanel(this);
        
        
    }
    
    
}